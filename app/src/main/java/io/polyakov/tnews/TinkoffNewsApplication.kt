package io.polyakov.tnews

import android.app.Application
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class TinkoffNewsApplication : Application() {

    override fun onCreate() {
        super.onCreate()

        startKoin {
            modules(applicationModule)
            androidContext(this@TinkoffNewsApplication)
        }
    }
}