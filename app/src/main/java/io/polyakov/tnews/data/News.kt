package io.polyakov.tnews.data

import java.util.*

data class News(
    val id: Int,
    val title: String,
    val publicationDate: Date
)