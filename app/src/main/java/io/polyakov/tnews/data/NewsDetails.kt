package io.polyakov.tnews.data

import java.util.*

data class NewsDetails(
    val news: News,
    val modificationDate: Date,
    val creationDate: Date,
    val content: String
)