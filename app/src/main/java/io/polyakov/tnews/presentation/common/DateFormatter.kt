package io.polyakov.tnews.presentation.common

import java.text.SimpleDateFormat
import java.util.*

class DateFormatter(locale: Locale) {
    private val dateFormat: SimpleDateFormat = SimpleDateFormat("EEE, d MMM yyyy", locale)
    fun format(date: Date) = dateFormat.format(date);
}