package io.polyakov.tnews.presentation.list

import android.content.Context
import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import io.polyakov.tnews.R
import io.polyakov.tnews.presentation.common.SimpleRecyclerAdapter

class NewsAdapter(context: Context, listener: OnItemClickListener<NewsItemPresentation>) :
    SimpleRecyclerAdapter<NewsAdapter.NewsViewHolder, NewsItemPresentation>(context, listener) {

    override fun getItemResLayout(): Int = R.layout.item_news

    override fun createHolder(view: View): NewsViewHolder =
        NewsViewHolder(view)

    override fun bindHolder(holder: NewsViewHolder, item: NewsItemPresentation) = with(item) {
        holder.date.text = date
        holder.title.text = title
    }

    class NewsViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val date: TextView = view.findViewById(R.id.item_news_date)
        val title: TextView = view.findViewById(R.id.item_news_title)
    }
}