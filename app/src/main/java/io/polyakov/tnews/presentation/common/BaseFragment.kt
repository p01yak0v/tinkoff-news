package io.polyakov.tnews.presentation.common

import android.os.Bundle
import androidx.fragment.app.Fragment
import org.koin.android.ext.android.inject

abstract class BaseFragment : Fragment() {

    protected val disposer: AutoDisposer by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        disposer.bindTo(lifecycle)
    }

}