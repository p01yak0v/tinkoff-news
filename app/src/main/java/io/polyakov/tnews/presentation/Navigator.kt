package io.polyakov.tnews.presentation

interface Navigator {
    fun openDetails(newsId: Int)
}