package io.polyakov.tnews.presentation.list

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import io.polyakov.tnews.R
import io.polyakov.tnews.presentation.Navigator
import io.polyakov.tnews.presentation.common.BaseFragment
import io.polyakov.tnews.presentation.common.SimpleRecyclerAdapter
import io.polyakov.tnews.presentation.utils.bindView
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import org.koin.androidx.viewmodel.ext.android.viewModel

class NewsFragment : BaseFragment(), SimpleRecyclerAdapter.OnItemClickListener<NewsItemPresentation> {

    private val newsList: RecyclerView by bindView(R.id.news_list)
    private val swipe: SwipeRefreshLayout by bindView(R.id.news_swipe)

    private val newsViewModel: NewsViewModel by viewModel()

    private lateinit var navigator: Navigator

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_news, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        navigator = activity as Navigator

        val adapter = NewsAdapter(requireContext(), this)
        adapter.updateItems(newsViewModel.cachedItems)
        newsList.adapter = adapter

        swipe.setOnRefreshListener {
            requestNews(false)
        }
    }

    override fun onStart() {
        super.onStart()
        requestNews(true)
    }

    private fun requestNews(cached: Boolean) {
        disposer += newsViewModel.getNews(cached)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(this::onNewsUpdated, this::onNewsError)

    }

    override fun onItemClick(position: Int, item: NewsItemPresentation) {
        navigator.openDetails(item.id)
    }

    private fun onNewsUpdated(news: List<NewsItemPresentation>) {
        swipe.isRefreshing = false
        newsList.newsAdapter.updateItems(news)
    }

    private fun onNewsError(throwable: Throwable) {
        swipe.isRefreshing = false
        Toast.makeText(context, R.string.news_error_unable_to_get_list, Toast.LENGTH_LONG).show()
    }

    private val RecyclerView.newsAdapter: NewsAdapter
        get() = adapter as NewsAdapter
}