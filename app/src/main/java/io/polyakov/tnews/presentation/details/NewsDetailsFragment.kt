package io.polyakov.tnews.presentation.details

import android.os.Bundle
import android.text.util.Linkify
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.core.os.bundleOf
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import io.polyakov.tnews.R
import io.polyakov.tnews.presentation.common.BaseFragment
import io.polyakov.tnews.presentation.utils.bindView
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import org.koin.androidx.viewmodel.ext.android.viewModel

class NewsDetailsFragment : BaseFragment() {

    private val titleLabel: TextView by bindView(R.id.news_details_title)
    private val createLabel: TextView by bindView(R.id.news_details_creation_date)
    private val modifyLabel: TextView by bindView(R.id.news_details_modification_date)
    private val publishLabel: TextView by bindView(R.id.news_details_publication_date)
    private val contentLabel: TextView by bindView(R.id.news_details_content)

    private val swipe: SwipeRefreshLayout by bindView(R.id.news_details_swipe)

    private val detailsViewModel: NewsDetailsViewModel by viewModel()

    private var newsId: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        newsId = arguments?.getInt(EXTRA_NEWS_ID) ?: throw IllegalArgumentException("${javaClass.name} have to accept $EXTRA_NEWS_ID as a parameter.")
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_details, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        swipe.setOnRefreshListener {
            requestNewsDetails(newsId, false)
        }
    }

    override fun onResume() {
        super.onResume()
        requestNewsDetails(newsId, true)
    }

    private fun requestNewsDetails(newsId: Int, cached: Boolean) {
        disposer += detailsViewModel.getNewsDetails(newsId, cached)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(this::onDetailsReceived, this::onDetailsError)
    }

    private fun onDetailsReceived(newsDetails: NewsDetailsPresentation) {
        with(newsDetails) {
            swipe.isRefreshing = false

            titleLabel.text = title

            createLabel.text = getString(R.string.news_details_create, createDate)
            modifyLabel.text = getString(R.string.news_details_modify, modifyDate)
            publishLabel.text = getString(R.string.news_details_publish, publishDate)

            contentLabel.text = content

        }
        Linkify.addLinks(contentLabel, Linkify.WEB_URLS)
    }

    private fun onDetailsError(throwable: Throwable) {
        swipe.isRefreshing = false
        Toast.makeText(context, R.string.news_error_unable_to_get_details, Toast.LENGTH_LONG).show()
    }

    companion object {
        private const val EXTRA_NEWS_ID = "news_id"

        fun createArgs(newsId: Int) = bundleOf(EXTRA_NEWS_ID to newsId)
    }

}