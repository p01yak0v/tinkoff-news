package io.polyakov.tnews.presentation.utils

import android.view.View
import androidx.annotation.IdRes
import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.Observer
import androidx.lifecycle.OnLifecycleEvent
import io.polyakov.tnews.presentation.Navigator

fun Fragment.bindNavigator(): Lazy<Navigator> = lazy { activity as Navigator }

fun <T : View> Fragment.bindView(@IdRes id: Int): Lazy<T> =
    LifecycleAwareLazy(this) {
        view?.findViewById<T>(id) ?: throw IllegalStateException("No view with passed ID has been found.")
    }

private class LifecycleAwareLazy<out T : View>(
    private val fragment: Fragment,
    private val initializer: (Fragment) -> T
) : Lazy<T>, LifecycleObserver {

    private var _value: T? = null

    init {
        fragment.viewLifecycleOwnerLiveData.observe(fragment, Observer {
            it.lifecycle.addObserver(this)
        })
    }

    override val value: T
        get() {
            if (!isInitialized()) {
                _value = initializer.invoke(fragment)
            }

            return _value as T
        }

    override fun isInitialized(): Boolean = _value != null

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    private fun onDestroyView() {
        _value = null
    }
}