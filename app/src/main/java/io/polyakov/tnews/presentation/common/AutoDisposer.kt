package io.polyakov.tnews.presentation.common

import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

class AutoDisposer : LifecycleObserver {
    private val compositeDisposable = CompositeDisposable()

    fun bindTo(lifecycle: Lifecycle) = lifecycle.addObserver(this)

    operator fun plusAssign(disposable: Disposable) {
        compositeDisposable.add(disposable)
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    fun destroy() {
        compositeDisposable.clear()
    }
}