package io.polyakov.tnews.presentation.list

import androidx.lifecycle.ViewModel
import io.polyakov.tnews.data.News
import io.polyakov.tnews.domain.NewsRepository
import io.polyakov.tnews.presentation.common.DateFormatter
import io.polyakov.tnews.presentation.utils.asSpannedHtml
import io.reactivex.Single

class NewsViewModel(
    private val newsRepository: NewsRepository,
    private val dateFormatter: DateFormatter
) : ViewModel() {

    private val _cachedItems: MutableList<NewsItemPresentation> = mutableListOf()
    val cachedItems: List<NewsItemPresentation>
        get() = _cachedItems

    fun getNews(cached: Boolean = false): Single<List<NewsItemPresentation>> {
        val newsSource= if (cached) {
            newsRepository.getNews()
        } else {
            newsRepository.getFreshNews()
        }
        return newsSource.map(this::toPresentation)
            .doOnSuccess(this::updateLocalCache)
    }

    private fun updateLocalCache(items: List<NewsItemPresentation>) {
        _cachedItems.apply {
            clear()
            addAll(items)
        }
    }

    private fun toPresentation(news: List<News>) : List<NewsItemPresentation> =
        news.map { it.toPresent() }

    private fun News.toPresent() = NewsItemPresentation(id, title.asSpannedHtml(), dateFormatter.format(publicationDate))

}