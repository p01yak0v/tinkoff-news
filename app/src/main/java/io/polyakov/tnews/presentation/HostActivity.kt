package io.polyakov.tnews.presentation

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import io.polyakov.tnews.R
import io.polyakov.tnews.presentation.details.NewsDetailsFragment
import io.polyakov.tnews.presentation.list.NewsFragment

class HostActivity : AppCompatActivity(), Navigator {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        showFragment(NewsFragment::class.java, addToBackStack = false)
    }

    override fun openDetails(newsId: Int) {
        showFragment(NewsDetailsFragment::class.java, NewsDetailsFragment.createArgs(newsId))
    }

    private fun <T : Class<out Fragment>> showFragment(fragmentClass: T, args: Bundle? = null, addToBackStack: Boolean = true) {
        var fragment = supportFragmentManager.findFragmentByTag(fragmentClass.name)
        if (fragment == null) {
            fragment = fragmentClass.newInstance()
        }

        fragment?.let { f ->
            if (f.isAdded) return

            f.arguments = args

            supportFragmentManager.beginTransaction().apply {
                add(R.id.main_fragment_host, f, fragmentClass.name)
                if (addToBackStack) {
                    addToBackStack(fragmentClass.name)
                }
                commit()
            }
        }
    }
}
