package io.polyakov.tnews.presentation.details

import android.text.Spanned

data class NewsDetailsPresentation(
    val title: Spanned,
    val createDate: String,
    val publishDate: String,
    val modifyDate: String,
    val content: Spanned
)