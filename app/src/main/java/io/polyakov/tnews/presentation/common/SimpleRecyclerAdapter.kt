package io.polyakov.tnews.presentation.common

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.recyclerview.widget.RecyclerView

abstract class SimpleRecyclerAdapter<ViewHolder : RecyclerView.ViewHolder, in ItemPresentation>(
        context: Context,
        private val itemListener: OnItemClickListener<ItemPresentation>? = null
) : RecyclerView.Adapter<ViewHolder>() {

    private val inflater = LayoutInflater.from(context)
    private val items: MutableList<ItemPresentation> = mutableListOf()

    @LayoutRes abstract fun getItemResLayout(): Int
    abstract fun createHolder(view: View): ViewHolder
    abstract fun bindHolder(holder: ViewHolder, item: ItemPresentation)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = inflater.inflate(getItemResLayout(), parent, false)
        return createHolder(view)
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        bindHolder(holder, items[position])

        itemListener?.run {
            holder.itemView.setOnClickListener {
                onItemClick(position, items[position])
            }
        }
    }

    fun updateItems(newItems: List<ItemPresentation>) {
        items.apply {
            clear()
            addAll(newItems)
        }
        notifyDataSetChanged()
    }

    interface OnItemClickListener<Item> {
        fun onItemClick(position: Int, item: Item)
    }
}