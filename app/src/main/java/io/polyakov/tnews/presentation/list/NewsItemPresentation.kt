package io.polyakov.tnews.presentation.list

import android.text.Spanned

data class NewsItemPresentation(
    val id: Int,
    val title: Spanned,
    val date: String
)