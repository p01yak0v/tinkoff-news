package io.polyakov.tnews.presentation.details

import androidx.lifecycle.ViewModel
import io.polyakov.tnews.data.NewsDetails
import io.polyakov.tnews.domain.NewsRepository
import io.polyakov.tnews.presentation.common.DateFormatter
import io.polyakov.tnews.presentation.utils.asSpannedHtml
import io.reactivex.Single

class NewsDetailsViewModel(
    private val repository: NewsRepository,
    private val dateFormatter: DateFormatter
) : ViewModel() {

    fun getNewsDetails(newsId: Int, cached: Boolean = true): Single<NewsDetailsPresentation> {
        val detailsObservable = if (cached) {
            repository.getNewsDetails(newsId)
        } else {
            repository.getFreshNewsDetails(newsId)
        }
        return detailsObservable.map { it.toPresent() }
    }


    private fun NewsDetails.toPresent() = NewsDetailsPresentation(
        news.title.asSpannedHtml(),
        dateFormatter.format(creationDate),
        dateFormatter.format(news.publicationDate),
        dateFormatter.format(modificationDate),
        content.asSpannedHtml()
    )

}