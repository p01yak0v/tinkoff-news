package io.polyakov.tnews.presentation.utils

import android.os.Build
import android.text.Html

fun String.asSpannedHtml() = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
    Html.fromHtml(this, Html.FROM_HTML_MODE_COMPACT)
} else {
    @Suppress("DEPRECATION")
    Html.fromHtml(this)
}