package io.polyakov.tnews

import android.annotation.SuppressLint
import io.polyakov.tnews.domain.NewsRepository
import io.polyakov.tnews.domain.NewsRepositoryImpl
import io.polyakov.tnews.domain.net.NetworkDriver
import io.polyakov.tnews.domain.net.retrofit.RetrofitNetworkDriver
import io.polyakov.tnews.domain.net.retrofit.TinkoffApiService
import io.polyakov.tnews.domain.storage.StorageDriver
import io.polyakov.tnews.domain.storage.room.RoomStorageDriver
import io.polyakov.tnews.presentation.common.AutoDisposer
import io.polyakov.tnews.presentation.common.DateFormatter
import io.polyakov.tnews.presentation.details.NewsDetailsViewModel
import io.polyakov.tnews.presentation.list.NewsViewModel
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.*

@SuppressLint("ConstantLocale")
val applicationModule = module {

    single { NewsRepositoryImpl(get(), get()) as NewsRepository }

    single { RoomStorageDriver(get()) as StorageDriver }

    single { RetrofitNetworkDriver(get()) as NetworkDriver}

    factory { Retrofit.Builder()
        .baseUrl(TinkoffApiService.BASE_URL)
        .addConverterFactory(GsonConverterFactory.create())
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .client(OkHttpClient.Builder()
                .addInterceptor(HttpLoggingInterceptor()
                    .apply { level = HttpLoggingInterceptor.Level.BODY }
                )
                .build())
        .build()
    }

    factory { AutoDisposer() }
    factory { DateFormatter(Locale("ru", "RU")) }

    viewModel { NewsViewModel(get(), get()) }
    viewModel { NewsDetailsViewModel(get(), get()) }
}