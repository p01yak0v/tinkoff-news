package io.polyakov.tnews.domain.net.retrofit

import io.polyakov.tnews.domain.net.retrofit.response.NewsDetailsRemoteRecord
import io.polyakov.tnews.domain.net.retrofit.response.NewsRemoteRecord
import io.polyakov.tnews.domain.net.retrofit.response.TinkoffBaseResponse
import io.reactivex.Single
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface TinkoffApiService {

    companion object {
        const val BASE_URL = "https://api.tinkoff.ru/v1/"

        private const val PATH_NEWS = "news"
        private const val PATH_CONTENT = "news_content"

        private const val QUERY_ID = "id"
    }

    @GET(PATH_NEWS) fun getNews(): Single<Response<TinkoffBaseResponse<List<NewsRemoteRecord>>>>
    @GET(PATH_CONTENT) fun getNewsById(@Query(QUERY_ID) newsId: Int): Single<Response<TinkoffBaseResponse<NewsDetailsRemoteRecord>>>

}