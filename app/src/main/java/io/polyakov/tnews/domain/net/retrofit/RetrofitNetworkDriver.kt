package io.polyakov.tnews.domain.net.retrofit

import io.polyakov.tnews.domain.net.NetworkDriver
import io.polyakov.tnews.domain.net.NetworkDriverException
import io.polyakov.tnews.domain.net.retrofit.response.NewsDetailsRemoteRecord
import io.polyakov.tnews.domain.net.retrofit.response.NewsRemoteRecord
import io.polyakov.tnews.domain.net.retrofit.response.TinkoffBaseResponse
import io.reactivex.Single
import retrofit2.Response
import retrofit2.Retrofit
import io.polyakov.tnews.domain.net.retrofit.response.Date as TinkoffDate

class RetrofitNetworkDriver(retrofit: Retrofit) : NetworkDriver {

    private val apiService: TinkoffApiService = retrofit.create(TinkoffApiService::class.java)

    override fun getNews(): Single<List<NewsRemoteRecord>> =
        apiService.getNews().map { processResponse(it) }

    override fun getNewsById(newsId: Int): Single<NewsDetailsRemoteRecord> =
        apiService.getNewsById(newsId).map { processResponse(it) }

    @Throws(NetworkDriverException::class)
    private fun <T> processResponse(response: Response<TinkoffBaseResponse<T>>): T {
        if (isResponseSuccessful(response)) {
            return response.body()?.payload ?: throw NetworkDriverException()
        }

        throw NetworkDriverException()
    }

    private fun isResponseSuccessful(response: Response<out TinkoffBaseResponse<*>>): Boolean {
        if (response.isSuccessful) {
            val body = response.body()
            body?.run {
                return RESULT_OK == result
            }
        }
        return false
    }

    companion object {
        private const val RESULT_OK = "OK"
    }
}