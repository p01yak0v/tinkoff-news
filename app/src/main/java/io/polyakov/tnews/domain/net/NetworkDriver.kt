package io.polyakov.tnews.domain.net

import io.polyakov.tnews.domain.net.retrofit.response.NewsDetailsRemoteRecord
import io.polyakov.tnews.domain.net.retrofit.response.NewsRemoteRecord
import io.reactivex.Single

interface NetworkDriver {
    fun getNews(): Single<List<NewsRemoteRecord>>
    fun getNewsById(newsId: Int): Single<NewsDetailsRemoteRecord>
}