package io.polyakov.tnews.domain.net.retrofit.response

import com.google.gson.annotations.SerializedName

data class NewsRemoteRecord(
    @SerializedName("id") val id: Int,
    @SerializedName("name") val name: String,
    @SerializedName("text") val title: String,
    @SerializedName("publicationDate") val publicationDate: Date,
    @SerializedName("bankInfoTypeId") val bankInfoTypeId: Int
)
