package io.polyakov.tnews.domain.storage.room.dao

import androidx.room.*
import io.polyakov.tnews.domain.storage.room.entity.NewsLocalRecord
import io.reactivex.Completable
import io.reactivex.Single

@Dao
interface NewsDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addNews(news: List<NewsLocalRecord>): Completable

    @Update
    fun updateNews(news: NewsLocalRecord): Completable

    @Query("select * from news order by publish_date desc")
    fun getNews(): Single<List<NewsLocalRecord>>

    @Query("select * from news where news_id = :newsId and content != ''")
    fun getNewsById(newsId: Int): Single<NewsLocalRecord>
}