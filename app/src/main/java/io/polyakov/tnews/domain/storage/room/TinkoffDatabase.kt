package io.polyakov.tnews.domain.storage.room

import androidx.room.Database
import androidx.room.RoomDatabase
import io.polyakov.tnews.domain.storage.room.dao.NewsDao
import io.polyakov.tnews.domain.storage.room.entity.NewsLocalRecord

@Database(entities = [ NewsLocalRecord::class ], version = 1)
abstract class TinkoffDatabase : RoomDatabase() {
    abstract fun getNewsDao(): NewsDao
}