package io.polyakov.tnews.domain.storage.room

import android.content.Context
import androidx.room.Room
import io.polyakov.tnews.domain.storage.StorageDriver
import io.polyakov.tnews.domain.storage.room.entity.NewsLocalRecord
import io.reactivex.Completable
import io.reactivex.Single

class RoomStorageDriver(context: Context): StorageDriver {

    private val db: TinkoffDatabase = Room.databaseBuilder(context,
        TinkoffDatabase::class.java, "tinkoff").build()

    override fun getNews(): Single<List<NewsLocalRecord>> = db.getNewsDao().getNews()

    override fun getNewsById(newsId: Int): Single<NewsLocalRecord> = db.getNewsDao().getNewsById(newsId)

    override fun addNews(news: List<NewsLocalRecord>): Completable = db.getNewsDao().addNews(news)

    override fun updateNews(news: NewsLocalRecord): Completable = db.getNewsDao().updateNews(news)
}