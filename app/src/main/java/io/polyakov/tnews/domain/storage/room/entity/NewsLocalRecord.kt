package io.polyakov.tnews.domain.storage.room.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "news")
data class NewsLocalRecord constructor(

    @PrimaryKey @ColumnInfo(name = "news_id") var id: Int,

    @ColumnInfo(name = "title") var title: String,
    @ColumnInfo(name = "publish_date") var publishDate: Long,
    @ColumnInfo(name = "create_date") var createDate: Long = 0,
    @ColumnInfo(name = "modify_date") var modifyDate: Long = 0,
    @ColumnInfo(name = "content") var content: String = ""
)