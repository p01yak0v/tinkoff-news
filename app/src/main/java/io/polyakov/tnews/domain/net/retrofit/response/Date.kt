package io.polyakov.tnews.domain.net.retrofit.response

import com.google.gson.annotations.SerializedName

data class Date(
    @SerializedName("milliseconds") val milliseconds: Long
)