package io.polyakov.tnews.domain

import android.util.Log
import android.util.SparseArray
import androidx.core.util.contains
import androidx.core.util.set
import io.polyakov.tnews.data.News
import io.polyakov.tnews.data.NewsDetails
import io.polyakov.tnews.domain.net.NetworkDriver
import io.polyakov.tnews.domain.net.retrofit.response.NewsDetailsRemoteRecord
import io.polyakov.tnews.domain.net.retrofit.response.NewsRemoteRecord
import io.polyakov.tnews.domain.storage.StorageDriver
import io.polyakov.tnews.domain.storage.room.entity.NewsLocalRecord
import io.reactivex.Single
import java.util.*
import io.polyakov.tnews.domain.net.retrofit.response.Date as TinkoffDate


class NewsRepositoryImpl(
    private val networkDriver: NetworkDriver,
    private val storageDriver: StorageDriver
) : NewsRepository {

    private val cachedNews: MutableList<News> = mutableListOf()
    private val cachedNewsDetails: SparseArray<NewsDetails> = SparseArray()

    override fun getNews(): Single<List<News>> = if (cachedNews.isNotEmpty()) {
        Single.just(cachedNews)
    } else {
        getCachedNews().flatMap { list ->
            if (list.isEmpty()) {
                getFreshNews()
            } else {
                Single.just(list)
            }
        }
    }

    override fun getFreshNews(): Single<List<News>> = networkDriver.getNews()
        .map { remoteRecords -> remoteRecords.sortedByDescending { it.publicationDate.milliseconds } }
        .flatMap(this::storeNews)
        .map { newsList -> newsList.map(this::remoteRecordToNews) }
        .doOnSuccess(this::updateNewsInMemoryCache)

    private fun getCachedNews(): Single<List<News>> = storageDriver.getNews()
        .map { localNews -> localNews.map(this::localRecordToNews)}
        .doOnSuccess(this::updateNewsInMemoryCache)

    private fun updateNewsInMemoryCache(news: List<News>) {
        cachedNews.clear()
        cachedNews.addAll(news)
    }

    override fun getNewsDetails(newsId: Int): Single<NewsDetails> =
        if(cachedNewsDetails.contains(newsId)) {
            Single.just(cachedNewsDetails[newsId])
        } else {
            getCachedNewsDetails(newsId)
                .flatMap { Single.just(it) }
                .onErrorResumeNext {  t: Throwable -> getFreshNewsDetails(newsId) }
        }


    override fun getFreshNewsDetails(newsId: Int): Single<NewsDetails> = networkDriver.getNewsById(newsId)
        .flatMap(this::updateStoredNewsWithDetails)
        .map(this::remoteRecordToDetails)
        .doOnSuccess(this::updateNewsDetailsInMemoryCache)

    private fun updateNewsDetailsInMemoryCache(newsDetails: NewsDetails) {
        cachedNewsDetails[newsDetails.news.id] = newsDetails
    }

    private fun getCachedNewsDetails(newsId: Int): Single<NewsDetails> = storageDriver.getNewsById(newsId)
        .map(this::localRecordToDetails)
        .doOnSuccess(this::updateNewsDetailsInMemoryCache)


    private fun storeNews(news: List<NewsRemoteRecord>?): Single<List<NewsRemoteRecord>> =
        if (news.isNullOrEmpty()) {
            Single.just(listOf())
        } else {
            storageDriver.addNews(news.map(this::remoteNewsToLocal))
                .andThen(Single.just(news))
        }

    private fun remoteNewsToLocal(remoteNews: NewsRemoteRecord): NewsLocalRecord =
        remoteNews.run { NewsLocalRecord(id, title, publicationDate.milliseconds) }

    private fun updateStoredNewsWithDetails(newsDetails: NewsDetailsRemoteRecord): Single<NewsDetailsRemoteRecord> =
        storageDriver.updateNews(newsDetails.toLocal()).doOnComplete {
            Log.d("UPDATE", "done")
        }.andThen(Single.just(newsDetails))

    private fun remoteRecordToDetails(remoteDetails: NewsDetailsRemoteRecord): NewsDetails =
        remoteDetails.run { NewsDetails(remoteRecordToNews(news), modificationDate.toPlatform(), creationDate.toPlatform(), content) }

    private fun NewsDetailsRemoteRecord.toLocal() = NewsLocalRecord(news.id, news.title,
        news.publicationDate.milliseconds, creationDate.milliseconds, modificationDate.milliseconds, content)

    private fun remoteRecordToNews(remoteNews: NewsRemoteRecord): News =
        remoteNews.run { News(id, title, publicationDate.toPlatform()) }

    private fun localRecordToNews(localRecord: NewsLocalRecord): News =
        localRecord.run { News(id, title, Date(publishDate)) }

    private fun localRecordToDetails(localRecord: NewsLocalRecord): NewsDetails =
        localRecord.run { NewsDetails(localRecordToNews(localRecord), Date(modifyDate), Date(createDate), content) }

    private fun TinkoffDate.toPlatform() = Date(milliseconds)
}