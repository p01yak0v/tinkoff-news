package io.polyakov.tnews.domain.storage

import io.polyakov.tnews.domain.storage.room.entity.NewsLocalRecord
import io.reactivex.Completable
import io.reactivex.Single

interface StorageDriver {

    fun getNews(): Single<List<NewsLocalRecord>>
    fun getNewsById(newsId: Int): Single<NewsLocalRecord>

    fun addNews(news: List<NewsLocalRecord>): Completable
    fun updateNews(news: NewsLocalRecord): Completable

}