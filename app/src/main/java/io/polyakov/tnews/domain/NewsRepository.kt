package io.polyakov.tnews.domain

import io.polyakov.tnews.data.News
import io.polyakov.tnews.data.NewsDetails
import io.reactivex.Single

interface NewsRepository {

    fun getFreshNews(): Single<List<News>>
    fun getNews(): Single<List<News>>

    fun getNewsDetails(newsId: Int): Single<NewsDetails>
    fun getFreshNewsDetails(newsId: Int): Single<NewsDetails>

}