package io.polyakov.tnews.domain.net.retrofit.response

import com.google.gson.annotations.SerializedName

data class NewsDetailsRemoteRecord(
    @SerializedName("title") val news: NewsRemoteRecord,
    @SerializedName("creationDate") val creationDate: Date,
    @SerializedName("lastModificationDate") val modificationDate: Date,
    @SerializedName("content") val content: String,
    @SerializedName("bankInfoTypeId") val bankInfoTypeId: Int,
    @SerializedName("typeId") val typeId: String
)