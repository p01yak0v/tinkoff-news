package io.polyakov.tnews.domain.net.retrofit.response

import com.google.gson.annotations.SerializedName

class TinkoffBaseResponse<T>(
    @SerializedName("resultCode") val result: String? = null,
    @SerializedName("payload") val payload: T? = null
)